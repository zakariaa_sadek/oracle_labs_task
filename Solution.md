# DevOps - Create a Docker image with a simple web application

## Solution

### 1. Set up your Docker environment.
### 2. To build your docker image,run this command:
```bash
 docker build -t test.
```
### 3. To start your docker container,run this command:
```bash
 docker run -d -p 9090:443 test:latest
```

### 3. Curl your web application by running:
```bash
 curl -k https://localhost:9090  
```
* **NB:** The -k flag is to turn off curl cert verification .


### 4. Enter to your browser and go to https://localhost:9090/ : 

**NB:**

 - You will find a Certificate Error (Since we're using a Self Signed Cerificate)

![](./images/SSL_Certificate_Error.png)



 - Ignore it and you will get a web Page like this:

![](./images/screenshot.png)


### 5. To test the web application 10.000 times, with 10 concurrent connections,run :
```bash
 abs -n 10000 -c 10 https://localhost:9090/ >> output.txt 
```

 - See the output of the file in output.txt


# Weaknesses of my Dockerfile :

I think that my Dockerfile is not perfect yet , but i'm sure the the Self Signed certificates that i created is not suitable for production , so we need to purchase a certificate from a trusted Certificate Authority.

# Maintainer
**Zakariaa SADEK**