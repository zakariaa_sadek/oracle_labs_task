# DevOps - Create a Docker image with a simple web application

## Exercise

This exercise is split in several subtasks. We are curious to see where you feel most comfortable and where you struggle.

### 1. Run the container and test:
* Base the image on "oraclelinux".
* Use apache as web server.
* Deploy a simple web application like : https://github.com/igameproject/Breakout.
* Add https support by integrating haproxy as reverse-proxy. Self-signed certificates are sufficient.
* The Dockerfile must build reproducible images.

### 2. Run the container and test:
* Open the web service in a browser and make a screenshot.
* Test a https URL of the web application 10 000 times, with 10 concurrent connections. Document the output.


### 3. Document the weaknesses of your Dockerfile:
- For example does the Dockerfile include simplifications, which are not suitable for a production application?
